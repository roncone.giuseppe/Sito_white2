// Scrivi un programma che dato:
// a) Un numero totale di gatti
// b) Il  numero dei gatti presenti in ogni fila
// Restituisca in output:
// - Il  numero di file risultanti
// - indicare il numero di gatti mancanti per completare una nuova fila 
// Gatti: 44, gatti in ogni fila : 6 


// let gatti = 44;
// let gatti_in_fila = 6;

// let file = Math.floor(gatti / gatti_in_fila); 

// let gatti_calc = file * gatti_in_fila;

// let gatti_avanzati = gatti - gatti_calc;

// let gatti_mancanti = gatti_in_fila - gatti_avanzati;

// console.log (gatti_avanzati);

// console.log(`ci sono ${file} file di gatti e ne mancano ${ gatti_mancanti}, con un avanzo di ${gatti_avanzati}`)


// ES - 4

//   Scrivi un programma che dati sette valori relativi alle temperature della settimana
//   stabilisca la giornata più calda e quella più fredda.
  Esempio:

// a = 10, b = -2, c = 31, d = 22, e = 15, f = -6, g = 7

// let caldo = Math.max(a, b, c, d, e, f, g)
// console.log(`La temperatura più calda é ${caldo}`)

// let freddo = Math.min (a, b, c, d, e, f, g)  
// console.log(`La temperatura più fredda é ${freddo}`)

// “La temperatura piu’ calda e’ 31 quella piu’ fredda -6”


// ES - 5

 
//   Scrivi un programma che dato un numero intero compreso tra 1 e 7
//   visualizzi il corrispondente giorno della settimana. Sapendo che:
//     1 = lunedì
//     2 = martedì
//     3 = mercoledì
//     ...
//     7 = domenica
//   Esempi:
//     Input: numero = 6
//     Output: "Sabato"
//     Input: numero = 10
//     Output: “Errore! Giorno della settimana non valido!"


// let giorno_della_settimana = prompt ("inserisci il tuo numero da 1 - 7")

// if (giorno_della_settimana == "1") {
//     alert("Lunedi")
// }
// if (giorno_della_settimana == "2") {
//     alert("martedi")
// }
// if (giorno_della_settimana == "3") {
//     alert("mercoledi")
// }
// if (giorno_della_settimana == "4") {
//     alert("giovedi")
// }
// if (giorno_della_settimana == "5") {
//     alert("venerdi")
// }
// if (giorno_della_settimana == "6") {
//     alert("sabato")
// }
// if (giorno_della_settimana == "7") {
//     alert("domenica")
// }
// if (giorno_della_settimana >= 8) {
//     alert("Errore! Giorno della settimana non valido!")
// }


// ES - 6

//   Scrivi un programma che converta un voto numerico (v) in un giudizio seguendo questi parametri:
//     v < 18: insufficiente
//     18 <= v < 21: sufficiente
//     21 <= v < 24: buono
//     24 <= v < 27: distinto
//     27 <= v <= 29: ottimo
//     v = 30: eccellente
//   Esempio:if
//     Input: v = 29
//     Output: Ottimo

// let voto = 20;

// if (voto < 18 && voto > 0) {
//     console.log ('insufficiente')
// } 
// else if  (voto <= 18 && voto < 21) {
//     console.log ('sufficiente')
// }else if (voto <= 21 && voto < 24) {
//     console.log ('buono')
// }else if (voto <= 24 && voto < 27) {
//         console.log ('distinto')
// }else if (voto <= 27 && voto <= 29) {
//     console.log ('Ottimo')
// }else if (voto == 30) {
//     console.log ('ECCELLENTE!')}
// else {
//     console.log('Numero non valido')
// }



// ES - 7

//  Scrivere un programma che, dato il numero dei tiri da effettuare per ciascun giocatore (N),
//   simuli un gioco di dadi tra due utenti, stampando il giocatore che ha totalizzato più punti,
//   supponendo che ogni dado abbia al massimo 6 facce, ogni giocatore tirerà il dado N volte, ciò  significa che verrà generato un numero casuale ad ogni tiro che sarà sommato ai precedenti per calcolare il punteggio del giocatore                                                                    
  

// // Suggerimento 
// // Usiamo questa formula per generare un numero random
// script.js
// Math.floor(Math.random() * (max - min + 1) + min);

// let n = prompt("tiri: ");
// let utente1=0;
// let utente2=0;

// function getRandomInt() {
//     return Math.floor(Math.random(1,2,3,4,5,6) * 24);
//   }
//   console.log(getRandomInt());

// let giocatore_a = getRandomInt

// let punteggio_a = (n_tiri * giocatore_a)

// let n_tiri2 = 4;
// function getRandomInt() {
//     return Math.floor(Math.random(1,2,3,4,5,6) * 24);
//   }
//   console.log(getRandomInt());



// let giocatore_b = getRandomInt

// let punteggio_b = (n_tiri2 * giocatore_b)

// let vincitore = Math.max(punteggio_a, )



// FATTO BENE

// let n = prompt("tiri: ");
// let utente1=0;
// let utente2=0;

// for(let i=1; i<=n; i++){

//     utente1 +=  Math.floor(Math.random() * (6 - 1 + 1) + 1);
//     utente2 +=  Math.floor(Math.random() * (6 - 1 + 1) + 1);

// }

// if(utente1 > utente2){
//     console.log("ha vinto 1");
// } else if (utente1 < utente2){
//     console.log("ha vinto 2");
// } else {
//     console.log("pareggio");
// }

// console.log(utente1);
// console.log(utente2);



